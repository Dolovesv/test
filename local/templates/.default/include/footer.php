<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

IncludeTemplateLangFile(__FILE__);?>
<div class="ft_footer">
<div class="ft_container">
    <div class="ft_about">
        <h4><?echo GetMessage("ABOUT_SHOP")?></h4>
        <ul>
            <li><a href="">Отзывы</a></li>
            <li><a href="">Контакты</a></li>
            <li><a href="">Руководство</a></li>
            <li><a href="">История</a></li>
        </ul>
    </div>
    <div class="ft_catalog">
        <h4><?echo GetMessage("ABOUT_SHOP")?></h4>
        <ul>
            <li><a href="">Кухни</a></li>
            <li><a href="">Кровати и кушетки</a></li>
            <li><a href="">Гарнитуры</a></li>
            <li><a href="">Тумобчки и прихожие</a></li>
            <li><a href="">Спальни и матрасы</a></li>
            <li><a href="">Аксессуары</a></li>
            <li><a href="">Столы и стулья</a></li>
            <li><a href="">Каталоги мебели</a></li>
            <li><a href="">Раскладные диваны</a></li>
            <li><a href="">Кресла</a></li>
        </ul>

    </div>
    <div class="ft_contacts">
        <h4><?echo GetMessage("CATALOG_T")?></h4>
        <!-- vCard        http://help.yandex.ru/webmaster/hcard.pdf      -->
        <p class="vcard">
						<span class="adr">
							<span class="street-address"><?echo GetMessage("ADRESS")?>
                                </span>
						</span>
            <span class="tel"><?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    ".default",
                    array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "COMPONENT_TEMPLATE" => ".default",
                        "PATH" => "/local/templates/.default/include/phone.php"
                    ),
                    false
                );?></span>
            <strong><?echo GetMessage("WORK_TIME")?></strong> <br/> <span class="workhours"><?$APPLICATION->IncludeComponent("bitrix:main.include", "time_work_header", Array(
                    "AREA_FILE_SHOW" => "file",	// Показывать включаемую область
                    "AREA_FILE_SUFFIX" => "inc",
                    "EDIT_TEMPLATE" => "",	// Шаблон области по умолчанию
                    "COMPONENT_TEMPLATE" => ".default",
                    "PATH" => "/local/templates/.default/include/time_work_head.php",	// Путь к файлу области
                ),
                    false
                );?></span><br/>
        </p>
        <ul class="ft_solcial">
            <li><a href="" class="fb"></a></li>
            <li><a href="" class="tw"></a></li>
            <li><a href="" class="ok"></a></li>
            <li><a href="" class="vk"></a></li>
        </ul>
        <div class="ft_copyright"><?echo GetMessage("COPI")?></div>


    </div>

    <div class="clearboth"></div>
</div>
</div>