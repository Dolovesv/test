<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <?$APPLICATION->ShowHead();?>

    <title><?//$APPLICATION->ShowTitle()?></title>
    <meta charset="windows-1251">

    <?
    $APPLICATION->AddHeadScript('/local/templates/.default/js/jquery-1.8.2.min.js');// для скриптов
    $APPLICATION->AddHeadScript('/local/templates/.default/js/slides.min.jquery.js');// для скриптов
    $APPLICATION->AddHeadScript('/local/templates/.default/js/jquery.carouFredSel-6.1.0-packed.js');// для скриптов
    $APPLICATION->AddHeadScript('/local/templates/.default/js/functions.js');// для скриптов

    $APPLICATION->SetAdditionalCSS('/local/templates/.default/template_style.css'); // для стилей
    ?>

</head>
<body>


<?if($USER->IsAdmin()):?>
    <?$APPLICATION->ShowPanel()?>
<?endif?>
<div class="wrap">
    <div class="hd_header_area">
       <?php include_once($_SERVER['DOCUMENT_ROOT']."/local/templates/.default/include/header.php") ?>
    </div>

    <!--- // end header area --->
    <?$APPLICATION->IncludeComponent(
        "bitrix:breadcrumb",
        "nav_template",
        Array(
            "PATH" => "",
            "SITE_ID" => "s1",
            "START_FROM" => "0"
        )
    );?>
    <br>
    <div class="main_container page">
        <div class="mn_container">
            <div class="mn_content">
                <div class="main_post">
                    <div class="main_title">
                        <p class="title"><?$APPLICATION->ShowTitle(false)?></p>
                    </div>
                    <!-- workarea -->

                    <!-- workarea -->

